﻿namespace SuperAxeExplorer.Managers
{
    public static class EventManager
    {
        #region Generic events classes
        #region One parameter callbacks events
        public class Event<T1>
        {
            public delegate void Handler(T1 element);
            private event Handler _event;

            public static Event<T1> operator +(Event<T1> e, Handler h)
            {
                e._event += h;
                return e;
            }

            public static Event<T1> operator -(Event<T1> e, Handler h)
            {
                e._event -= h;
                return e;
            }

            public void Fire(T1 element) {
                if (_event != null) {
                    _event(element);
                }
            }
        }
        #endregion

        #region Two parameters callbacks events
        public class Event<T1, T2>
        {
            public delegate void Handler(T1 element1, T2 element2);
            public event Handler _event;

            public static Event<T1, T2> operator +(Event<T1, T2> e, Handler h)
            {
                e._event += h;
                return e;
            }

            public static Event<T1, T2> operator -(Event<T1, T2> e, Handler h) {
                e._event -= h;
                return e;
            }

            public void Fire(T1 element1, T2 element2)
            {
                if (_event != null)
                {
                    _event(element1, element2);
                }
            }
        }
    #endregion
        #endregion

        public static Event<Actor> onActorPopped = new Event<Actor>();
        public static Event<Actor> onActorDied = new Event<Actor>();
        public static Event<Actor> onActorHit = new Event<Actor>();
        public static Event<Actor> onActorAttack = new Event<Actor>();
        public static Event<Actor> onActorUsedActive = new Event<Actor>();
        public static Event<Actor, Item> onActorUsedItem = new Event<Actor, Item>();
        public static Event<Actor, Item> onActorLootItem = new Event<Actor, Item>();
    }
}
