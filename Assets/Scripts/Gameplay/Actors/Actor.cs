﻿using UnityEngine;

namespace SuperAxeExplorer
{
    using Managers;

    public abstract class Actor : MonoBehaviour {
        private int _health;
        public int Health
        {
            get { return _health; }
            private set {
                _health = value;
                if (!IsAlive) {
                    EventManager.onActorDied.Fire(this);
                }
            }
        }

        public bool IsAlive
        {
            get { return Health > 0; }
        }
    }
}
